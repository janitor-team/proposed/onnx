onnx (1.12.0-2) unstable; urgency=medium

  [ Mo Zhou ]
  * dch: ack previous nmu

  [ Francis Murtagh ]
  * Add missing onnx.proto file to -dev package

 -- Mo Zhou <lumin@debian.org>  Thu, 07 Jul 2022 21:58:27 -0700

onnx (1.12.0-1) unstable; urgency=medium

  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Sat, 25 Jun 2022 20:07:32 -0700

onnx (1.12.0-1~exp1) experimental; urgency=medium

  * New upstream version 1.12.0
  * Rebase existing patches.
  * Add the missing test dependency python3-numpy.
  * Refresh existing patches.
  * Overhaul d/rules to fixup builds.
  * Upload to experimental.

 -- Mo Zhou <lumin@debian.org>  Thu, 23 Jun 2022 21:33:16 -0700

onnx (1.7.0+dfsg-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Patch: Python 3.10 support. (Closes: #1004952)

 -- Stefano Rivera <stefanor@debian.org>  Sat, 26 Mar 2022 17:25:56 -0400

onnx (1.7.0+dfsg-3) unstable; urgency=medium

  * Update patch headers and lintian overrides.
  * d/*.install: don't require dh-exec for those do not use any feature.

 -- Mo Zhou <lumin@debian.org>  Tue, 26 Jan 2021 13:35:24 +0800

onnx (1.7.0+dfsg-2) unstable; urgency=medium

  [ Christian Kastner ]
  * Demangle symbols file

  [ Mo Zhou ]
  * d/rules: Fix FTBFS bug with multiple python3. (Closes: #972014)
  * Stop tracking the C++ symbols. (Closes: #971204)
  * Amend test data handling when there are multiple versions of python3.
  * Update maintainer mail address to debian-ai@l.d.o.

 -- Mo Zhou <lumin@debian.org>  Sat, 17 Oct 2020 13:22:23 +0800

onnx (1.7.0+dfsg-1) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * modernize watch file

  [ Mo Zhou ]
  * New upstream version 1.7.0+dfsg (Closes: #963245)
  * Remove the merged 2482.patch and refresh soversion.patch.
  * Update installation paths following upstream cmake changes.
  * Refresh symbols control file.

 -- Mo Zhou <lumin@debian.org>  Sat, 25 Jul 2020 14:30:31 +0800

onnx (1.6.0+dfsg-3) unstable; urgency=medium

  * Make the symbols tracking file amd64-only.

 -- Mo Zhou <lumin@debian.org>  Wed, 29 Apr 2020 21:30:37 +0800

onnx (1.6.0+dfsg-2) unstable; urgency=medium

  * Apply wrap-and-sort.
  * Add symbols control file.
  * Upload to unstable.

 -- Mo Zhou <lumin@debian.org>  Wed, 29 Apr 2020 09:10:28 +0800

onnx (1.6.0+dfsg-1) experimental; urgency=medium

  * Initial release. (Closes: #915011)

 -- Mo Zhou <lumin@debian.org>  Tue, 28 Apr 2020 12:08:04 +0800
